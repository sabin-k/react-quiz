import styled from 'styled-components';

export const Wrapper = styled.div`
  max-width: 1100px;
  min-width: 550px;
  background-color: #dbe7e4;
  border-radius: 4px;
  border: 2px solid #83c5be;
  padding: 20px;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.16);
  text-align: center;
  p {
    font-size: 18px;
  }
`;

type ButtonWrapperProps = {
  correct: boolean;
  userClicked: boolean;
};

export const ButtonWrapper = styled.div<ButtonWrapperProps>`
  transition: all 0.3s ease;
  :hover {
    opacity: 0.8;
  }
  button {
    cursor: pointer;
    user-select: none;
    font-size: 16px;
    width: 100%;
    height: 40px;
    margin: 5px 0;
    background: ${({ correct, userClicked }) =>
    correct
      ? '#2ecc71'
      : !correct && userClicked
        ? '#e74c3c'
        : 'linear-gradient(175deg, #B7F8DB, #50A7C2)'};
    border: 1px solid #fff;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.16);
    border-radius: 4px;
    color: #fff;
    text-shadow: 0px 1px 0px rgba(0, 0, 0, 0.16);
  }
`;