import styled, { createGlobalStyle } from "styled-components";
import BGImage from "./images/minator-yang.jpg";

export const GlobalStyle = createGlobalStyle`
  html {
    height: 100%;
  }
  body {
    background-image: url(${BGImage});
    background-size: cover;
    margin: 0;
    padding: 0 20px;
    display: flex;
    justify-content: center;
  }
  * {
  font-family: 'Montserrat', sans-serif;
  box-sizing: border-box;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  > p {
    color: #fff;
  }
  .score {
    color: #fff;
    font-size: 30px;
  }
  h1 {
    font-family: "Montserrat", sans-serif;
    background-color: #dbe7e4;
    font-weight: 400;
    background-size: 100%;
    background-clip: text;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    -moz-background-clip: text;
    -moz-text-fill-color: transparent;
    font-size: 60px;
    text-align: center;
    margin: 20px;
  }
  .start,
  .next {
    cursor: pointer;
    background: linear-gradient(180deg, #b5e48c, #99d98c);
    border: 2px solid #d9ed92;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.16);
    border-radius: 4px;
    height: 40px;
    margin: 20px 0;
    padding: 0 40px;
    color: #1e6091;
    font-size: 16px;
    font-weight: 500;
  }
  .start {
    max-width: 200px;
  }
  .version{
    position: absolute;
    bottom: 0px
  }
`;
